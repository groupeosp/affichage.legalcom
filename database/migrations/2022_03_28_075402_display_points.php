<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_points', function (Blueprint $table) {
            $table->id();
            $table->integer('order');
            $table->bigInteger('project_id')->unsigned();
            $table->bigInteger('support_id')->unsigned();
            $table->bigInteger('format_id')->unsigned();
            $table->string('name', 50);
            $table->string('latitude', 50)->nullable();
            $table->string('longitude', 50)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('postcode', 45);
            $table->string('country', 45);
            $table->string('city', 45);
            $table->bigInteger('assigned_to')->nullable();
            $table->bigInteger('posed_by')->nullable();
            $table->dateTime('posed_at')->nullable();
            $table->bigInteger('removed_by')->nullable();
            $table->dateTime('removed_at')->nullable();
            $table->bigInteger('created_by');
            $table->dateTime('created_at');
            $table->bigInteger('updated_by')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('support_id')->references('id')->on('supports')->onDelete('cascade');
            $table->foreign('format_id')->references('id')->on('formats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('display_points');
    }
};
