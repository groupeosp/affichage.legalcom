<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->unsigned();
            $table->string('name', 255);
            $table->dateTime('date_begin');
            $table->dateTime('date_end');
            $table->tinyInteger('management_bailiff');
            $table->tinyInteger('billed');
            $table->bigInteger('billed_by')->nullable();
            $table->tinyInteger('billed_at')->nullable();
            $table->bigInteger('created_by');
            $table->dateTime('created_at');
            $table->bigInteger('updated_by');
            $table->dateTime('updated_at');
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
