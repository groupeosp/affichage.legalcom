<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_step_display_point', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project_step_id')->unsigned();
            $table->bigInteger('display_point_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->tinyInteger('checked');
            $table->bigInteger('checked_by');
            $table->dateTime('checked_at');
            $table->tinyInteger('status');
            $table->foreign('project_step_id')->references('id')->on('project_step')->onDelete('cascade');
            $table->foreign('display_point_id')->references('id')->on('display_points')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_step_display_point');
    }
};
