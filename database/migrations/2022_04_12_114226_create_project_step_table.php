<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_step', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project_id')->unsigned();
            $table->bigInteger('step_id')->unsigned();
            $table->bigInteger('assigned_to');
            $table->dateTime('date_begin');
            $table->dateTime('date_end');
            $table->tinyInteger('status');
            $table->bigInteger('created_by');
            $table->dateTime('created_at');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('step_id')->references('id')->on('steps')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_step');
    }
};
