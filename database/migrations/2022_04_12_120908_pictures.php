<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project_step_display_point_id')->unsigned();
            $table->string('hashname');
            $table->string('geoloc');
            $table->bigInteger('created_by');
            $table->dateTime('created_at');
            $table->foreign('project_step_display_point_id')->references('id')->on('project_step_display_point')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');
    }
};
