<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProjectStepDisplayPoint>
 */
class ProjectStepDisplayPointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sort = rand(1,4);
        $sort2 = rand(1,2);
        return [
            'project_step_id' => $sort,
            'display_point_id' => $sort2,
            'user_id' => 1,
            'checked' => 0,
            'checked_by' => 1,
            'checked_at' => now(),
            'status' => $sort,
        ];
    }
    
}
