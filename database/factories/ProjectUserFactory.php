<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProjectUser>
 */
class ProjectUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sort = rand(1,5);
        $sortid = rand(1,10);
        return [
            'project_id' => $sortid,
            'courier_id' => $sort,
            'created_by' => $sort,
            'created_at' => now(),
        ];
    }
}
