<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProjectContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sortid = rand(1,10);
        $contact = rand(1,20);
        return [
            'contact_id' => $contact,
            'project_id' => $sortid,
            
        ];
    }
    
}
