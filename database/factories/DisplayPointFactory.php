<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DisplayPoint>
 */
class DisplayPointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $support = rand(1, 10);
        $format = rand(1, 4);
        $lang = 33.7490;
        $long = -84.3880;
        return [
            'order' => rand(1, 10),
            'project_id' => 1,
            'support_id' => $support,
            'format_id' => $format,
            'name' => $this->faker->company(),
            'latitude' => $this->faker->latitude(
                $min = ($lang * 10000 - rand(0, 50)) / 10000,
                $max = ($lang * 10000 + rand(0, 50)) / 10000
            ),
            'longitude' => $this->faker->longitude(
                $min = ($lang * 10000 - rand(0, 50)) / 10000,
                $max = ($lang * 10000 + rand(0, 50)) / 10000
            ),
            'address' => $this->faker->address(),
            'postcode' => $this->faker->postcode(),
            'country' => 'France',
            'city' => $this->faker->city(),
            'posed_by' => 1,
            'posed_at' => now(),
            'removed_by' => 1,
            'removed_at' => now(),
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now(),
        ];
    }
}
