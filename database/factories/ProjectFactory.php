<?php

namespace Database\Factories;

use Brick\Math\BigInteger;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sort = rand(1,5);
        $sort2 = rand(1,10);
        return [
            'name' => $this->faker->word(),
            'status' => $this->faker->randomElement(['Proposition', '1er Affichage', 'Contrôle', 'Terminé']),
            'display_date' => $this->faker->date(),
            'user_assigned_id' => $sort,
            'customer_id' => $sort2,
            'created_at' => now(),
            'updated_at' => now(),
            'created_by' => $sort,
            'updated_by' => $sort,
        ];
    }
}
