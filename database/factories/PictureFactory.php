<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Picture>
 */
class PictureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sort = rand(1,10);
        return [
            'project_step_display_point_id' => $sort,
            'hashname' => Hash::make('nom_image'),
            'geoloc' => $this->faker->address(),
            'created_by' => 1,
            'created_at' => now(),
        ];
    }
}
