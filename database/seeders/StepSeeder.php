<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Step;

class StepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Step::create([
            'id'               => 1,
            'name'             => 'Proposition cartographie affichage',
            'role'             => 'commercial',
        ]);
        Step::create([
            'id'               => 2,
            'name'             => 'Affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 3,
            'name'             => 'Livraison dossiers enquête',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 4,
            'name'             => 'Livraison affiches',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 5,
            'name'             => 'Contrôle 1 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 6,
            'name'             => 'Contrôle 2 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 7,
            'name'             => 'Contrôle 3 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 8,
            'name'             => 'Contrôle 4 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 9,
            'name'             => 'Contrôle 5 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 10,
            'name'             => 'Contrôle 6 affichage',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 11,
            'name'             => 'Contrôle 1 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 12,
            'name'             => 'Contrôle 2 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 13,
            'name'             => 'Contrôle 3 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 14,
            'name'             => 'Contrôle 4 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 15,
            'name'             => 'Contrôle 5 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 16,
            'name'             => 'Contrôle 6 registres papier',
            'role'             => 'afficheur',
        ]);
        Step::create([
            'id'               => 17,
            'name'             => 'Contrôle 1 huissier',
            'role'             => 'huissier',
        ]);
        Step::create([
            'id'               => 18,
            'name'             => 'Contrôle 2 huissier',
            'role'             => 'huissier',
        ]);
        Step::create([
            'id'               => 19,
            'name'             => 'Contrôle 3 huissier',
            'role'             => 'huissier',
        ]);
        Step::create([
            'id'               => 20,
            'name'             => 'Contrôle 4 huissier',
            'role'             => 'huissier',
        ]);
        Step::create([
            'id'               => 21,
            'name'             => 'Dépose',
            'role'             => 'afficheur',
        ]);
}}
