<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ProjectUser;

class ProjectUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectUser::create([
            'id'               => 1,
            'project_id'                  => 1,
            'user_id'             => 3,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectUser::create([
            'id'               => 2,
            'project_id'                  => 1,
            'user_id'             => 7,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectUser::create([
            'id'               => 3,
            'project_id'                  => 1,
            'user_id'             => 6,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectUser::create([
            'id'               => 4,
            'project_id'                  => 1,
            'user_id'             => 5,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
    }
}
