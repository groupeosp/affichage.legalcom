<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Format;

class FormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Format::create([
            'id'               => 1,
            'name'             => 'A2 papier',
        ]);
        Format::create([
            'id'               => 2,
            'name'             => 'A2 plastifiée',
        ]);
        Format::create([
            'id'               => 3,
            'name'             => 'A3',
        ]);
        Format::create([
            'id'               => 4,
            'name'             => 'A4',
        ]);
    }
}
