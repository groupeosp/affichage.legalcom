<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ProjectStep;

class ProjectStepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectStep::create([
            'id'               => 1,
            'project_id'                  => 1,
            'step_id'             => 1,
            'date_begin'            => now(),
            'date_end'            => now(),
            'status'                   => 0,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectStep::create([
            'id'               => 2,
            'project_id'                  => 1,
            'step_id'             => 2,
            'date_begin'            => now(),
            'date_end'            => now(),
            'status'                   => 0,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectStep::create([
            'id'               => 3,
            'project_id'                  => 1,
            'step_id'             => 3,
            'date_begin'            => now(),
            'date_end'            => now(),
            'status'                   => 0,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
        ProjectStep::create([
            'id'               => 4,
            'project_id'                  => 1,
            'step_id'             => 4,
            'date_begin'            => now(),
            'date_end'            => now(),
            'status'                   => 0,
            'created_by'            => 1,
            'created_at'            => now(),
        ]);
    }
}
