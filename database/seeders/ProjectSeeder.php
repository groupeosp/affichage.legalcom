<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::create([
            'id'               => 1,
            'customer_id'                  => 1,
            'name'             => 'Rénovation Rer E',
            'date_begin'            => now(),
            'date_end'            => now(),
            'management_bailiff'      => 0,
            'billed'                  => 0,
            'billed_by'            => null,
            'billed_at'            => null,
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
    }
}
