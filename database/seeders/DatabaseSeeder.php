<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Database\Factories\ProjectFactory;
use Database\Factories\UserFactory;
use Database\Factories\ProjectUserFactory;
use Database\Factories\ProjectContactFactory;
use Database\Factories\CustomerFactory;
use Database\Factories\CustomerContactFactory;
use Database\Factories\DisplayPointFactory;
use Database\Factories\PictureFactory;
use Database\Factories\ProjectStepDisplayPoint;
use Database\Factories\Picture;



use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SupportSeeder::class);
        $this->call(FormatSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(CustomerUserSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(StepSeeder::class);
        $this->call(ProjectStepSeeder::class);
        $this->call(ProjectUserSeeder::class);
        

        // \App\Models\User::factory(10)
        // ->create();
    }
}
