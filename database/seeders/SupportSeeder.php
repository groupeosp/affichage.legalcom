<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Support;

class SupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Support::create([
            'id'               => 1,
            'name'             => 'grille',
        ]);
        Support::create([
            'id'               => 2,
            'name'             => 'lampadaire',
        ]);
        Support::create([
            'id'               => 3,
            'name'             => 'panneau d\'interdiction',
        ]);
        Support::create([
            'id'               => 4,
            'name'             => 'panneau directionnel',
        ]);
        Support::create([
            'id'               => 5,
            'name'             => 'piquet bois',
        ]);
        Support::create([
            'id'               => 6,
            'name'             => 'poteau de rue',
        ]);
        Support::create([
            'id'               => 7,
            'name'             => 'poteau éléctrique',
        ]);
        Support::create([
            'id'               => 8,
            'name'             => 'poteau de stationnement',
        ]);
        Support::create([
            'id'               => 9,
            'name'             => 'panneau administratif sécurisé',
        ]);
        Support::create([
            'id'               => 10,
            'name'             => 'panneau administratif extérieur',
        ]);
    }
}
