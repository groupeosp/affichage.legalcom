<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'id'               => 1,
            'name'             => 'OSP',
            'created_by'             => 1,
            'created_at'             => now(),
            'updated_by'             => 1,
            'updated_at'             => now(),
        ]);
        Customer::create([
            'id'               => 2,
            'name'             => 'SGP',
            'created_by'             => 1,
            'created_at'             => now(),
            'updated_by'             => 1,
            'updated_at'             => now(),
        ]);
    }
}
