<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'               => 1,
            'name'                  => 'Harfouche',
            'firstname'             => 'Juba',
            'email'                 => 'jharfouche@osp.fr',
            'role'                  => 'admin',
            'phone_number'            => '0149040164',
            'password'              => Hash::make('OspCie89'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 2,
            'name'                  => 'Guy',
            'firstname'             => 'Jeremy',
            'email'                 => 'jguy@osp.fr',
            'role'                  => 'admin',
            'phone_number'            => '0149040160',
            'password'              => Hash::make('OspCie60'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 3,
            'name'                  => 'Lecourt',
            'firstname'             => 'Paul',
            'email'                 => 'plecourt@osp.fr',
            'role'                  => 'commercial',
            'phone_number'            => '0149040153',
            'password'              => Hash::make('OspCie53'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 4,
            'name'                  => 'Besançon',
            'firstname'             => 'Séverine',
            'email'                 => 'sbesancon@osp.fr',
            'role'                  => 'commercial',
            'phone_number'            => '0149040152',
            'password'              => Hash::make('OspCie52'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 5,
            'name'                  => 'Bache',
            'firstname'             => 'Laurent',
            'email'                 => 'lquenel@osp.fr',
            'role'                  => 'afficheur',
            'phone_number'            => '0149040161',
            'password'              => Hash::make('OspCie61'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 6,
            'name'                  => 'Huissman',
            'firstname'             => 'John',
            'email'                 => 'huissier@osp.fr',
            'role'                  => 'huissier',
            'phone_number'            => '0149040161',
            'password'              => Hash::make('huissiermdp'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 7,
            'name'                  => 'Contactman',
            'firstname'             => 'Sylvain',
            'email'                 => 'contact@osp.fr',
            'role'                  => 'contact',
            'phone_number'            => '0149040161',
            'password'              => Hash::make('contactmdp'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
        User::create([
            'id'               => 8,
            'name'                  => 'Greslé',
            'firstname'             => 'Thomas',
            'email'                 => 'thomas@osp.fr',
            'role'                  => 'commercial',
            'phone_number'            => '0149040154',
            'password'              => Hash::make('ThomasOsp'),
            'last_connection'       => now(),
            'created_by'            => 1,
            'created_at'            => now(),
            'updated_by'            => 1,
            'updated_at'            => now(),
        ]);
    }
}
