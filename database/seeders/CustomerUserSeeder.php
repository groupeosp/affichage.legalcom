<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CustomerUser;

class CustomerUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 1,
        ]);
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 2,
        ]);
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 3,
        ]);
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 4,
        ]);
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 5,
        ]);
        CustomerUser::create([
            'customer_id'               => 1,
            'user_id'             => 6,
        ]);
        CustomerUser::create([
            'customer_id'               => 2,
            'user_id'             => 7,
        ]);
    }
}
