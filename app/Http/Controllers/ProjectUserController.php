<?php

namespace App\Http\Controllers;
use App\Models\ProjectUser;

use Illuminate\Http\Request;

class ProjectUserController extends Controller
{
    public function delete(Request $request){
        
        ProjectUser::where('id', $request->projectuser_id)->delete();
    }
}
