<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller

{
    public function index() {
        return Customer::all();
    }
    
    public function store(Request $request)
    {

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->created_by = Auth::id();
        $customer->created_at = now();
        $customer->updated_by = Auth::id();
        $customer->updated_at = now();
        $customer->save();
    }

    public function delete(Request $request){
        
        Customer::where('id', $request->customer_id)->delete();
    }

    public function update(Request $request)
    {

        Customer::where('id', $request->id)->update([
            'name' => $request->name,
        ]);
    }

}
