<?php

namespace App\Http\Controllers;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class NoteController extends Controller
{
    public function store(Request $request)
    {

        $note = new Note();
        $note->project_step_display_point_id = $request->projectstepdisplaypoint_id;
        $note->content = $request->content;
        $note->created_by = Auth::id();
        $note->created_at = now();
        $note->updated_at = now();
        $note->save();
    }

    public function delete(Request $request){
        
        Note::where('id', $request->note_id)->delete();
    }
    public function update(Request $request){
        
        Note::where('id', $request->note_id)->update([
            
            'content' => $request->content,
        ]);
    }
}
