<?php

namespace App\Http\Controllers;

use App\Models\ProjectStepDisplayPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectStepDisplayPointController extends Controller
{
    public function setChecked(Request $request) {
        ProjectStepDisplayPoint::where('id', $request->project_step_display_point_id)->update([
            
            'status' => $request->status,
            'checked_by' => Auth::id(),
            'checked_at' => now(),
            
        ]);
    }

    public function update(Request $request){
        
        Note::where('id', $request->note_id)->update([
            
            'content' => $request->content,
        ]);
    }
}
