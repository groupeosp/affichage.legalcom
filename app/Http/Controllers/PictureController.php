<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Picture;
use App\Models\ProjectStep;
use App\Models\ProjectStepDisplayPoint;
use Illuminate\Support\Facades\Storage;

class PictureController extends Controller
{
    public function store(Request $request)
    {

        $picture = new Picture(); 
        $picture->project_step_display_point_id = $request->project_step_display_point_id;
        $name = $request->file->store('public/'.$request->project_id); 
        $name = preg_replace('(public\/)' , '', $name);      
        $picture->hashname = $name;
        $picture->geoloc = $request->geoloc;
        $picture->created_by = Auth::id();
        $picture->created_at = now();
        $picture->save();

        ProjectStepDisplayPoint::where('id', $request->project_step_display_point_id)->update([
            'status' => 3,
            
        ]);
    }

    public function delete(Request $request){
        
        Picture::where('id', $request->picture_id)->delete();
        Storage::delete('public/'.$request->hashname);
    }

    
    public function getPath($pictureId){
        
        $picture = Picture::where('id', $pictureId)->get();
        $file_path = storage_path("app/" .'public/'. $picture[0]->hashname);
        return response()->file(
            $file_path,

            [

                'Content-Disposition' => 'inline; filename=' . 'test'

            ]

        );
    }
    public function testp($id, $dpid){
        $project_step_pending = ProjectStep::where('project_id', $id)->where('status', 0)->orderBy('date_begin', 'ASC')->first();
        $project_step_display_point = ProjectStepDisplayPoint::where('project_step_id' , $project_step_pending->id)->where('display_point_id', $dpid)->first();
        $picture = Picture::where('project_step_display_point_id', $project_step_display_point->id)->first();
       return $picture->hashname;
    }
}
 