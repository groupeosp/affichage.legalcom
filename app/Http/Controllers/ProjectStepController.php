<?php

namespace App\Http\Controllers;

use App\Models\ProjectStep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectStepController extends Controller
{
    public function store(Request $request)
    {
        $projectstep = new ProjectStep();
        $projectstep->project_id = $request->project_id;
        $projectstep->step_id = $request->step_id;
        $projectstep->assigned_to = $request->assigned_to;
        $projectstep->date_begin = $request->date_begin;
        $projectstep->status = 0;
        $projectstep->date_end = $request->date_end;
        $projectstep->created_by = Auth::id();
        $projectstep->created_at = now();
        $projectstep->save();
    }

    public function update(Request $request){
        
        $projectstep = ProjectStep::where('id', $request->id)->update([
            
            'date_begin' => $request->date_begin,
            'date_end' => $request->date_end,
            'status' => $request->status,

        ]);
        return $projectstep;
    }

    public function delete(Request $request){
        
        ProjectStep::where('id', $request->projectstep_id)->delete();
    }

    public function getCommercial(){
        return 1;
    }

}
