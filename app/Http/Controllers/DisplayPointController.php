<?php

namespace App\Http\Controllers;

use App\Models\DisplayPoint;
use App\Models\Step;
use App\Models\ProjectStep;
use App\Models\ProjectUser;
use App\Models\Format;
use App\Models\Picture;
use App\Models\User;
use App\Models\Project;
use App\Models\ProjectStepDisplayPoint;
use App\Models\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Raw;
use PDF;

class DisplayPointController extends Controller
{
    public function gotoDisplaypoints($id)
    {
        $project = Project::with('customer')->where('id', $id)->first();
        
        // $customer_id = Project::where('id', $id)->pluck('customer_id')->first();
        $contacts = User::join('customer_user', 'id', '=', 'user_id')->where('role', 'contact')->where('customer_id', $project->customer_id)->distinct()->get();
        
        $users_project = ProjectUser::with('users')->where('project_id', $id)->get();
        // $intervenants = User::join('customer_user', 'id', '=' , 'user_id')->where('role','!=', 'contact')->where('customer_id' , 1)->get();
        $intervenants = User::whereHas('customerUser', function ($query) {
            $query->where('customer_id', 1);
        })
            ->where('role', '!=', 'contact')->get();

            
        $request = DisplayPoint::with('project.customer')->with('project')->where('project_id', $id)->get();
        
        return view('displaypoints', ['project' => $project, 'contacts' => $contacts, 'users_project' => $users_project, 'intervenants' => $intervenants, 'request' => $request, 'id' => $id, 'customer_id' => $project->customer_id]);
    }

    public function gotoDisplaypoint($id, $dpid)
    {
        //étape en cours
        $project_steps = ProjectStep::with('steps', 'projectstepdisplaypoint.note', 'projectstepdisplaypoint.picture')->where('project_id', $id)->where('status', 0)->orderby('date_begin', 'ASC')->first();
        //verification si displaypoint existe
        

        //displaypoint
        // $display_point = DisplayPoint::with('projectstepdisplaypoint.projectstep.steps', 'projectstepdisplaypoint.picture', 'projectstepdisplaypoint.note')
        // ->where('id', $dpid)->get();
        $display_all = DisplayPoint::where('project_id', $id)->select('id', 'order')->orderBy('order', 'ASC')->get();
        $format = Format::all();
        $support = Support::all();
        $display_point = ProjectStepDisplayPoint::with('displaypoint', 'projectstep.steps', 'note' , 'picture')
        ->whereHas('projectstep', function($query) use ($project_steps) {
            $query->where('id' , $project_steps->id)->orwhere('status', 1);
         })
         ->where('display_point_id', $dpid)->get();
        return view('displaypoint', ['display_point' => $display_point, 'project_steps' => $project_steps, 'format' => $format, 'support' => $support, 'display_all' => $display_all,]);
    }
    
    public function getIdbyOrder(Request $request){
        //ensuite faire la meme chose pour -1 et retourner prev_order
        if($request->way == 'right') {
        $next_order = DisplayPoint::where('project_id', $request->display['project_id'])->where('order', $request->display['order'] + 1)->first();}
        else{
            $next_order = DisplayPoint::where('project_id', $request->display['project_id'])->where('order', $request->display['order'] - 1)->first();    }
        return response()->json([
            'next_order' => $next_order,
        ]);
    }

    public function getAllDp(Request $request)
    {
            $display_all = DisplayPoint::where('project_id', $request->project_id)->orderBy('order', 'ASC')->get();
            return response()->json([
                'display_all' => $display_all,
            ]);
    }

    public function getDp(Request $request)
    {
        $id = $request->dpid;
        $project_step_pending = ProjectStep::where('project_id', $id)->where('status', 0)->orderBy('date_begin', 'ASC')->first();
        
        $displaypoint_list = DisplayPoint::where('project_id', $id)->get();
        if($displaypoint_list){
        $displaypoint_list->toArray();
        foreach ($displaypoint_list as $value) {
            

            $displaypoint_exist = ProjectStepDisplayPoint::where('project_step_id', $project_step_pending->id)->where('display_point_id', $value->id)->get();

            if ($displaypoint_exist->count() == 0) {
                $displaypoint_exist = ProjectStepDisplayPoint::insert([
                    'project_step_id' => $project_step_pending->id,
                    'display_point_id' => $value->id,
                    'user_id' => 1,
                    'checked' => 0,
                    'checked_by' => 1,
                    'checked_at' => now(),
                    'status' => 0,
                ]);
            }
        }};
        if($project_step_pending){
        $displays = DisplayPoint::leftJoin('project_step_display_point', 'display_points.id', 'display_point_id')
        ->leftJoin('pictures', 'project_step_display_point.id', 'pictures.project_step_display_point_id')
        ->leftJoin('notes', 'project_step_display_point.id', 'notes.project_step_display_point_id')
        ->with('support')->with('format')->where('project_id', $id)
        ->where('project_step_display_point.project_step_id' , $project_step_pending->id)
        ->selectRaw("display_points.* , count(pictures.id) as nbPhoto, count(notes.id) as nbNote, project_step_display_point.status")
        ->groupBy('display_points.id')->orderBy('display_points.order', 'ASC')->paginate(10);
        }else{
            $displays = DisplayPoint::leftJoin('project_step_display_point', 'display_points.id', 'display_point_id')
        ->leftJoin('pictures', 'project_step_display_point.id', 'pictures.project_step_display_point_id')
        ->leftJoin('notes', 'project_step_display_point.id', 'notes.project_step_display_point_id')
        ->with('support')->with('format')->where('project_id', $id)
        ->selectRaw("display_points.* , count(pictures.id) as nbPhoto, count(notes.id) as nbNote, project_step_display_point.status")
        ->groupBy('display_points.id')->orderBy('display_points.order', 'ASC')->paginate(10);
        }
        $project_steps = ProjectStep::with('steps')->where('project_id', $id)->orderby('date_begin', 'ASC')->get();
        $formats = Format::all();
        $supports = Support::all();
        $step = Step::all();
        
        
        if (!$project_step_pending){
            $project_step_display_points = false;
        }else{
            $project_step_display_points = ProjectStepDisplayPoint::where('project_step_id', $project_step_pending->id)->get();
        };

        return response()->json([
            'displays' => $displays,
            'projectsteps' => $project_steps,
            'steps' => $step,
            'formats' => $formats,
            'supports' => $supports,
            'project_step_display_points' => $project_step_display_points,
        ]);
    }

    public function store(Request $request)
    {
        $count = DisplayPoint::where('project_id', $request->project_id)->count();
        $displaypoint = new DisplayPoint();
        $displaypoint->order= $count + 1;
        $displaypoint->project_id = $request->project_id;
        $displaypoint->support_id = $request->support_id;
        $displaypoint->format_id = $request->format_id;
        $displaypoint->name = $request->name;
        $displaypoint->latitude = $request->latitude;
        $displaypoint->longitude = $request->longitude;
        $displaypoint->address = $request->address;
        $displaypoint->postcode = $request->postcode;
        $displaypoint->country = "France";
        $displaypoint->city = $request->city;
        $displaypoint->assigned_to = $request->assigned_to;
        $displaypoint->posed_by = null;
        $displaypoint->posed_at = null;
        $displaypoint->removed_by = null;
        $displaypoint->removed_at = null;
        $displaypoint->created_by = Auth::id();
        $displaypoint->created_at = now();
        $displaypoint->updated_by = null;
        $displaypoint->updated_at = null;
        $displaypoint->save();
    }

    public function update(Request $request)
    {

        DisplayPoint::where('id', $request->id)->update([
            'support_id' => $request->support_id,
            'format_id' => $request->format_id,
            'name' => $request->name,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'postcode' => $request->postcode,
            'city' => $request->city,
        ]);
    }
    public function updateOrder(Request $request)
    {
        $display = $request->all();
        foreach($display['display'] as $key => $value){
            DisplayPoint::where('id', $value['id'])->update([
                'order' => $key + 1,
            ]);
        }
        
    }

    public function delete(Request $request)
    {

        DisplayPoint::where('id', $request->display_point_id)->delete();
    }

    public function viewPDF($id){
        $project_step_pending = ProjectStep::where('project_id', $id)->where('status', 0)->orderBy('date_begin', 'ASC')->first();
        $displays = DisplayPoint::leftJoin('project_step_display_point', 'display_points.id', 'display_point_id')
        ->leftJoin('pictures', 'project_step_display_point.id', 'pictures.project_step_display_point_id')->with('format', 'support')->where('project_id',$id)->orderBy('order','ASC')->get();
        // dd($displays);
        $project = Project::with('customer')->where('id', $id)->first();
  

        $data = [
            'displays' => $displays,
            'project' => $project,
            'project_step_pending' => $project_step_pending,
        ];
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE,
            ]
        ]);

        $pdf = \PDF::setOptions(['isHTML5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        $pdf->getDomPDF()->setHttpContext($contxt);
        $pdf = PDF::loadView('pdf.displayspdf', $data);
        return $pdf->stream();
    }
}
