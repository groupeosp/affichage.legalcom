<?php

namespace App\Http\Controllers;

use App\Models\CustomerUser;
use App\Models\ProjectUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;


class UserController extends Controller
{
    public function store(Request $request)
    {
        if($request->user_id == 0){
        $user = new User();
        $user->name = $request->name;
        $user->firstname = $request->firstname;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->phone_number = $request->phone_number;
        $user->password = Hash::make('azerty1234');
        $user->last_connection = null;
        $user->created_by = Auth::id();
        $user->created_at = now();
        $user->updated_by = Auth::id();
        $user->updated_at= now();
        $user->save();
        $project_user= new ProjectUser();
        $project_user->project_id = $request->pid;
        $project_user->user_id = $user->id;
        $project_user->created_by = Auth::id();
        $project_user->created_at = now();
        $project_user->save();
        $customer_user = new CustomerUser();
        $customer_user->customer_id = $request->cid;
        $customer_user->user_id = $user->id;
        $customer_user->save();
        return response()->json([
            'user_sucess' => $user,
        ]);
    }else{
        $project_user= new ProjectUser();
        $project_user->project_id = $request->pid;
        $project_user->user_id = $request->user_id;
        $project_user->created_by = Auth::id();
        $project_user->created_at = now();
        $project_user->save();
        $customer_user = new CustomerUser();
        $customer_user->customer_id = $request->cid;
        $customer_user->user_id = $request->user_id;
        $customer_user->save();
    }
    
    }

    public function update(Request $request)
    {

        User::where('id', $request->id)->update([
            'name' => $request->name,
            'firstname' => $request->firstname,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
        ]);
    }

    public function delete(Request $request){
        
        User::where('id', $request->user_id)->delete();
    }
}
