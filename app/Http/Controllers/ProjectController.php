<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use App\Models\Customer;
use App\Models\ProjectUser;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ProjectController extends Controller
{
    public function index()
    { 
        return view('project');
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function getProjects(Request $request)
    { 
        $req = $request->all();
        // Si recherche par nom
        if ($req) {
            // DB::connection()->enableQueryLog();

            $name = $req['name'] ? '%' . $req['name'] . '%' : '%%';
            $customer = $req['customer'] ? '%' . $req['customer'] . '%' : '%%';  

            $projects = Project::with(['users' => function ($query){
                $query->where('role', 'commercial');
                } ])->with('projectstep.steps')->whereHas('customer', function ($query) use ($customer){
                                 $query->where('name', 'like', $customer);
                                 })->with('customer')
                                ->where('name', 'LIKE', $name)
                                ->withCount('paff')
                                ->orderBy('created_at', 'desc')
                                ->paginate(5);
        }
        // Sinon retourner tous les contacts
        else {
            $projects = Project::with('customer')->withCount('paff')->paginate(5);
        }
        return response()->json([
            'projects' => $projects
        ]);
        
    }

    public function getCommercial(Request $request){
        $users_project = ProjectUser::whereHas('users', function($query) {
            $query->where('role', 'commercial')->get();
        })->where('project_id', $request->project_id)->get();
        return response()->json([
            'users_project' => $users_project
        ]);

    }
    public function create()
    {
        return [
            'users' => User::all(),
            'customers' => Customer::all(),
        ];
    }
    // public function create()
    // {
    //     return view('createproject', [
    //         'users' => User::all(),
    //         'customers' => Customer::all(),
    //     ]);
    // }


    public function store(Request $request)
    {
        $project = new Project();
        $project->customer_id = $request->customer_id;
        $project->name = $request->name;
        $project->date_begin = $request->date_begin;
        $project->date_end = $request->date_end;
        $project->management_bailiff = 0;
        $project->billed = 0;
        $project->billed_by = null;
        $project->billed_at = null;
        $project->created_by = Auth::id();
        $project->created_at = now();
        $project->updated_by = Auth::id();
        $project->updated_at = now();
        $project->save();
    }

    public function update(Request $request)
    {

        Project::where('id', $request->project_id)->update([
            'customer_id' => $request->customer_id,
            'name' => $request->name,
            'date_begin' => $request->date_begin,
            'date_end' => $request->date_end,
            'updated_by' => Auth::id(),
            'updated_at' => now(),
        ]);
    }

    public function delete(Request $request)
    {
        $project_check = Project::leftJoin('project_step', 'projects.id', 'project_step.project_id')->leftJoin('display_points', 'projects.id', 'display_points.project_id')->where('projects.id', $request->project_id)->select('display_points.id','project_step.id')->get();
        if(is_null($project_check[0]->id)){
        Project::where('id', $request->project_id)->delete();
        return response('true');
    }
        return response('false');
    }
}