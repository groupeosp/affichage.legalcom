<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerUser extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'customer_user';

    public function user(){

        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    public function customer(){

        return $this->belongsTo(Customer::class, 'customer_id' , 'id');
    }

    
    
}
