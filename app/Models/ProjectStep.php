<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProjectStepDisplayPoint;
use App\Models\Step;

class ProjectStep extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'project_step';
    

    public function steps(){

    return $this->belongsTo(Step::class, 'step_id', 'id');
    
    }

    public function projectstepdisplaypoint (){

        return $this->hasMany(ProjectStepDisplayPoint::class);
    }

}
