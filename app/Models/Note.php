<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{   
    use HasFactory;
    public $timestamps = false;

    public function projectstepdisplaypoint (){

        return $this->hasMany(ProjectStepDisplayPoint::class);
    }

    public function display()

    {

        return $this->belongsTo('App\Models\DisplayPoint', 'display_id' , 'id');

    }

   public function createdBy()

    {

        return $this->belongsTo('App\Models\User', 'created_by');

    }

    public function updatedBy()

    {

        return $this->belongsTo('App\Models\User', 'updated_by');

    }

}
