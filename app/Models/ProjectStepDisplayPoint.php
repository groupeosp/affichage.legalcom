<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use App\Models\DisplayPoint;
use App\Models\Note;
use App\Models\Picture;
use App\Models\ProjectStep;

class ProjectStepDisplayPoint extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'project_step_display_point';
    
    public function displaypoint(){
        return $this->belongsTo(DisplayPoint::class, 'display_point_id', 'id');
    }

    public function projectstep(){
        return $this->belongsTo(ProjectStep::class, 'project_step_id', 'id')->orderby('id', 'ASC');
    }  

    public function projectstep_encours(){
        return $this->belongsTo(ProjectStep::class, 'project_step_id', 'id')->where('status', 0)->orderBy('date_begin', 'ASC');
    }
    public function note(){
        return $this->hasMany(Note::class, 'project_step_display_point_id', 'id');
    }  

    public function picture(){
       return $this->hasMany(Picture::class, 'project_step_display_point_id', 'id');
    }
}
