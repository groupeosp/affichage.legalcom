<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DisplayPoint;

class Support extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function display()

    {

        return $this->hasMany(DisplayPoint::class);

    }
}
