<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Step;
use App\Models\ProjectStep;
use App\Models\User;

class Project extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['dpstatus'];

    public function getDpstatusAttribute(){
        return $this->steps()->orderByPivot('date_begin', 'ASC')->wherePivot('status', '!=', 1)->get(); // on récupère les étapes du projets en les triantspar date décroissantes, et on ne récupère que ceux en cours
    }



    public function users()
    {

        return $this->belongsToMany(User::class);

    }

    public function projectusers()
    {

        return $this->hasMany(ProjectUser::class);

    }



    public function projectstep()
    {

        return $this->hasMany(ProjectStep::class)->where('status' , 0)->orderBy('id', 'ASC');

    }

    public function steps()
    {

        return $this->belongsToMany(Step::class)->withPivot('date_begin', 'date_end');

    }


    public function courier()

    {

        return $this->belongsTo('App\Models\User', 'users_courier_id');

    }

    public function customer()

    {

        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');

    }

    public function paff()

    {

        return $this->hasMany('App\Models\DisplayPoint', 'project_id', 'id');

    }

    public function assignedTo()

    {

        return $this->belongsTo('App\Models\User', 'users_assigned_id', 'id');

    }

   public function createdBy()

    {

        return $this->belongsTo('App\Models\User', 'created_by');

    }

    public function updatedBy()

    {

        return $this->belongsTo('App\Models\User', 'updated_by');

    }
    
}
