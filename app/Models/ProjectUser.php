<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class ProjectUser extends Model
{
    public $timestamps = false;    
    use HasFactory;
    protected $table = 'project_user';

    public function users(){

        return $this->belongsTo(User::class, 'user_id' , 'id');
     
     }

     public function commercial(){

        return $this->belongsTo(User::class, 'project_id', 'id');
     
     }


    public function project()

    {

        return $this->belongsTo('App\Models\Projet', 'projects_id');

    }

    public function courier()

    {

        return $this->belongsTo('App\Models\User', 'courier_id');

    }

   public function createdBy()

    {

        return $this->belongsTo('App\Models\User', 'created_by');

    }


}
