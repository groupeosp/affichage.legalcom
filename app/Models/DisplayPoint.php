<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProjectStepDisplayPoint;

class DisplayPoint extends Model
{
    use HasFactory;
    public $timestamps = false;


    public function projectstepdisplaypoint (){

        return $this->hasMany(ProjectStepDisplayPoint::class);
    }

    public function project()

    {
        return $this->belongsTo(Project::class);
    }

    public function format()

    {

        return $this->belongsTo(Format::class, 'format_id','id');

    }

    public function support()

    {

        return $this->belongsTo(Support::class, 'support_id','id');

    }

   public function createdBy()

    {

        return $this->belongsTo('App\Models\User', 'created_by');

    }

    public function updatedBy()

    {

        return $this->belongsTo('App\Models\User', 'updated_by');

    }
}
