module.exports = {
   content: ["./storage/framework/views/*.php", "./resources/**/*.blade.php", "./resources/**/*.js", "./resources/**/*.vue"],
   darkMode: true, // or 'media' or 'class'
   theme: {
      extend: {
         colors: {
            // Configure your color palette here
             'bluesky':'#6CBAD1',
             'control':'#F65C5E',
             '1aff':'#5296B9',
             'legalcom': '#287DA2',
             'fontblue' : '#2B3F64',
            },
      },
   },
   variants: {
      extend: {},
   },
   plugins: [],
};
