@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('createproject')}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Projet') }}</div>
                <div>
                    @if(empty($projects))
                    <form method="POST" action="{{ route('createproject')}}">
                        @csrf

                        <input class="bg-gray-100" type="text" name="name" />
                        <select name="user_assigned_id">
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                            
                        </select>
                        <select name="customer_id">
                            @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                            @endforeach
                            
                        </select>

                        <input type="submit" />
                    </form>
                    @else
                    <form method="POST" action="{{ route('userCategoryUpdate',$category->id)}}">
                        @csrf
                        <input type="text" name="name" value="{{$category->name}}" />
                        <input type="text" name="description" value="{{$category->description}}" />
                        <input type="submit" />
                    </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection