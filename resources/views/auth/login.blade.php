@extends('layouts.app')

@section('content')
<body class="bg-white">
<div class="w-screen h-screen">
        <div class="w-4/5 h-4/5 translate-y-1/4 m-auto">
            <img src="{{ asset('images/logo_legal.png') }}" class="mx-auto mb-4"/>
                {{-- <div class="card-header text-center">{{ __('Connexion') }}</div> --}}

                <div class="mx-auto">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="flex flex-col items-center gap-4">
{{--                             
                            <label for="email" class="flex row">{{ __('Adresse email')
                                }}</label> --}}      
                                <div>                  
                                <input id="email" type="email" class="focus:outline-none focus:ring-2 focus:ring-bluesky indent-2.5 bg-neutral-200 w-72 h-14 rounded-full @error('email') is-invalid @enderror"
                                    name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror 
                                </div>   
                            {{-- <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password')
                                }}</label> --}}
                                <div>
                                <input id="password" type="password"
                                    class="focus:outline-none focus:ring-2 focus:ring-bluesky indent-2.5 bg-neutral-200 w-72 h-14 rounded-full @error('password') is-invalid @enderror" name="password"
                                    placeholder="Mot de passe" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                               </div>

                               
                                <div class="form-check">
                                    <input class="form-check-input " type="checkbox" name="remember" id="remember" {{
                                        old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Se souvenir de moi') }}
                                    </label>
                                </div>
                        
                        

                        
                        <div class="flex flex-col gap-4">
                                <button type="submit" class="bg-bluesky w-72 h-14 rounded-full text-white">
                                    {{ __('Connexion') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="text-gray-400 text-center" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié?') }}
                                </a>
                                @endif
                        </div>
                         </div>  
                    </form>
                </div>
                </div>
        </div>
</body>
@endsection