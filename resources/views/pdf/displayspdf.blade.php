<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PDF TEST</title>
   
   
    <style>
        @page {
            margin: 27mm 20mm 20mm 20mm !important;
            padding: 0px 0px 0px 0px !important;
            /* margin: 180px 50px; */
        }

        .pagenum:before { content: counter(page); }
        .header { position: fixed; left: 0px; top: -80px; right: 0px; width: 645px;}
        .header-block { float: left; width: 160px; height: 60px; text-align: center; display: block; vertical-align: middle; }
        .title-block {width:600px ; height: 40px; text-align: center; display: block; vertical-align: middle;}
        .header-banner {float: right; height: 60px; width: 475px; background-color: #287DA2;line-height :0.5; text-align: left; color: white; text-transform: uppercase; font-family:'helvetica', sans-serif; font-size: 14px; font-weight: 400;}
        .footer { position: fixed; left: 0px; bottom: -62px; right: 0px; height: 66px; background-color: #287DA2; }
        .footer .page:after { content: counter(page, upper-roman); }
        .page_break { page-break-before: auto; }

        table.FooterTable {
            width: 100%;
            text-align: left;
            border-collapse: collapse;
            line-height: 0.25rem;
            color: white;
            font-family:'helvetica', sans-serif; font-size: 14px; font-weight: 400;
            }
            table.FooterTable td, table.FooterTable th {
            
            }
            table.FooterTable tbody td {
            font-size: 14px;
            }
            table.FooterTable tfoot td {
            font-size: 14px;
            }
            table.FooterTable tfoot .links {
            text-align: right;
            }
            table.FooterTable tfoot .links a{
            display: inline-block;
            background: #1C6EA4;
            color: #FFFFFF;
  
            border-radius: 5px;
        }
        
    </style>
    </head>

   <body class="page">
    <header class="header">
        <div>
        <div class="header-block"><div style="position: relative;"><img src="{{public_path('/images/logo_legal2.png')}}" width="160px" height="53px" style="position: absolute; top: 50%;
            transform: translateY(-50%);"></div></div>
        <div class="header-banner">
            <p style="margin-left: 10px">{{$project->customer->name}}&nbsp;-&nbsp;{{$project->name}}</p>
            <p style="margin-left: 10px; font-size: 12px;">{{$project_step_pending->steps->name}}&nbsp;({{ date('d-m-Y', strtotime($project_step_pending->date_begin))}} 
                - {{ date('d-m-Y', strtotime($project_step_pending->date_end))}})</p>
        
        
        </div> </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    </header>
    <footer class="footer"> 
        <table class="FooterTable">
            <tbody>
            <tr>
            <td>
            <p>LEGALCOM</p>
            <p>14 rue Beffroy Neuilly-Sur-Seine 92200</p>
            <p>T&eacute;l: 0149040152 - 0149040153</p>
            </td>
            <td >Page <label class="pagenum"></label></td>
            </tr>
            </tbody>
            </table>
    </footer>
    <main >
        
        
{{--         
        <img src="{{ storage_path('app/public/1/3gVotp6bMkWd8sSt5drVHDmf8JkfAG5Tn0KJhZpK.jpg') }}" width="300px" height="228px">
        <h1 style="text-align: center;">test</h1> --}}
    @foreach ($displays as $item)
    @if(($item->project_step_id) == $project_step_pending->id)
        <div style="margin-top: 14.5px">
        <div class="title-block">
        <div style="position: relative;"><img src="{{public_path('/images/billboard.png')}}" width="36px" height="36px" style="position: absolute; top: 50%;
        transform: translateY(-50%);"><p style="color: #287DA2; font-weight:600">{{$item->order}}&nbsp;-&nbsp;{{$item->postcode}}&nbsp;-&nbsp;{{$item->city}}&nbsp;-&nbsp;{{$item->name}}&nbsp;-&nbsp;{{$item->address}}</p></div></div>
        <div>
            <p>
                Support : {{$item->support->name}}
                &nbsp;&nbsp;&nbsp;&nbsp;
                Format : {{$item->format->name}}
            </p>
        </div>
        @if ($item->hashname)
            <img src="{{ storage_path('app/public/'.$item->hashname) }}" style="max-width: 600px; height: 366px;">
        @else
            <img src="{{ public_path('/images/nopicture.jpg') }}" style="max-width: 600px; height: 366px;">
        @endif
       </div>
       <div class="page_break"></div>
       @endif
    @endforeach
    
    </main>
    <script type="text/php">
        if ( isset($pdf) ) { 

            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}",
                            $font, 6, array(0,0,0));
           
        }
        </script>
   </body>


</html>