@extends('layouts.app')

@section('content')
{{-- {{ Breadcrumbs::render('home', $id)}} --}}


<displaypointlist-component :project='@json($project)' :item='@json($request)' :users_project='@json($users_project)' :intervenants='@json($intervenants)' :id='@json($id)' :customer_id='@json($customer_id)' :contacts='@json($contacts)'></displaypointlist-component>
@endsection