@extends('layouts.app')

@section('content')


<displaypoint-component :project_steps='@json($project_steps)' :display='@json($display_point)' :format='@json($format)' :support='@json($support)' :display_all='@json($display_all)'></displaypoint-component>

@endsection