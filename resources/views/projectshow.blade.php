@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Projets') }}</div>
                {{-- <div>
                    <a class="nav-link" href="{{ route('userCategoryForm') }}">+</a>
                </div> --}}
                <!-- <b>Categories</b> -->
                <ul>
                    @foreach ($projects as $project)
                    <li>{{$project->name}}
                        {{-- <a href="{{route('userCategoryForm', $category->id)}}">update</a>
                        <a href="{{route('userCategoryDelete', $category->id)}}">delete</a> --}}
                    </li>
                    @endforeach
                </ul>
                <a href="{{route('createproject')}}"></a>
            </div>
        </div>
    </div>
</div>
</div>




@endsection