<?php
 
use App\Models\Project;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;
use App\Http\Controllers\ProjectController;
use SebastianBergmann\Environment\Console;

Breadcrumbs::for('project', function (BreadcrumbTrail $trail) {
    $trail->push('Projets', route('project'));
});


Breadcrumbs::for('home', function (BreadcrumbTrail $trail, $id) {
    $trail->parent('project');
    $trail->push('Home', route('gotod', $id));
});

Breadcrumbs::for('createproject', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('createproject', route('createproject'));
});
