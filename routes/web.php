<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DisplayPointController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectStepController;
use App\Http\Controllers\ProjectStepDisplayPointController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectUserController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\PictureController;
use App\Models\DisplayPoint;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (! auth()->check()) {
        return view('auth.login');;
    }
    return view('project');
});
Route::get('/logout', [ProjectController::class, 'logout'])->name('lg');

Route::get('/map', function () {
    return view('map');
});

Route::get('/welcome', function () {
    return view('welcome');
});
Auth::routes();




Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => 'IsAdmin'], function () {
    Route::get('/projects', [App\Http\Controllers\ProjectController::class, 'index'])->name('project');
    Route::post('/projects', [ProjectController::class, 'getProjects']);
    Route::post('/displays', [DisplayPointController::class, 'getDp']);
    Route::post('/projectupdate', [ProjectController::class, 'update']);
    Route::post('/projectdelete', [ProjectController::class, 'delete']);

    //STORAGE
    Route::get('/picture/{pictureId?}/', [PictureController::class, 'getPath']);
    // Route::get('/projects/{id}/displaypoints', [DisplayPointController::class, 'gotoDisplaypoint'])->name('gotod');
    Route::get('/projects/{id}/displaypoints', [DisplayPointController::class, 'gotoDisplaypoints'])->name('gotod');
    Route::get('/projects/{id}/displaypoints/{dpid}', [DisplayPointController::class, 'gotoDisplaypoint']);

    // Route::get('projects/{id}/displaypoints', function () { 
    //     return view ('displaypoints');
    // })->name('display');

    Route::get('test', function () {
        return view('createproject');
    })->name('createproject');

    // Route::resource('projects', ProjectController::class);
    Route::get('/picture/{id}/{dpid}', [PictureController::class, 'testp']);
    Route::get('/picture/{pictureId?}/', [PictureController::class, 'getPath']);

    Route::post('/userupdate', [UserController::class, 'update']);

    Route::post('/projectcreate', [ProjectController::class, 'store']);
    Route::post('/projectstepcreate', [ProjectStepController::class, 'store']);
    Route::post('/projectstepupdate', [ProjectStepController::class, 'update']);
    Route::post('/projectstepdelete', [ProjectStepController::class, 'delete']);
    Route::post('/projectgetcommercial', [ProjectStepController::class, 'getCommercial']);



    Route::post('/psdpsetchecked', [ProjectStepDisplayPointController::class, 'setChecked']);

    Route::post('/projectuserdelete', [ProjectUserController::class, 'delete']);
  
    Route::get('/customerget', [CustomerController::class, 'index']);
    Route::post('/customercreate', [CustomerController::class, 'store']);
    Route::post('/customerdelete', [CustomerController::class, 'delete']);
    Route::post('/customerupdate', [CustomerController::class, 'update']);

    Route::post('/displaypointcreate', [DisplayPointController::class, 'store']);
    Route::post('/displaypointupdate', [DisplayPointController::class, 'update']);
    Route::post('/displaypointdelete', [DisplayPointController::class, 'delete']);
    Route::post('/displayall', [DisplayPointController::class, 'getAllDp']);
    Route::post('/updateorder', [DisplayPointController::class, 'updateOrder']);
    Route::post('/getorder', [DisplayPointController::class, 'getIdbyOrder']);
    Route::get('/pdf/{id}', [DisplayPointController::class, 'viewPDF'])->name('displayspdf');


    Route::post('/notecreate', [NoteController::class, 'store']);
    Route::post('/notedelete', [NoteController::class, 'delete']);
    Route::post('/noteupdate', [NoteController::class, 'update']);

    Route::post('/picturecreate', [PictureController::class, 'store']);
    Route::post('/picturedelete', [PictureController::class, 'delete']);
    Route::post('/picturepath', [PictureController::class, 'getPath']);

    Route::post('/contactcreate', [UserController::class, 'store']);
    Route::post('/userdelete', [UserController::class, 'delete']);


    Route::post('/projectform', [ProjectController::class, 'create']);
});
